<%-- 
    Document   : setupSchedule
    Created on : 22 Sep, 2016, 5:53:35 PM
    Author     : Srishti Miglani
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<Html>
    <Head>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="<c:url value='/js/datetimepicker.js'/>"> </script>

  </script>
    <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
    <fmt:setBundle basename="ApplicationResources" />
    <title> <fmt:message key="title.manageschedule"/> </title>    
    </Head>   
    <body>
        <form action="${pageContext.request.contextPath}/nocturne/modifyschedule" method="post">
        <h1> <fmt:message key= "label.manageschedule.setup"/></h1>
        <h3> <fmt:message key="label.manageschedule.assignedBy" /> </h3>
<select name= "users">
   <c:choose>
     <c:when  test="${param['linkPressed'] == 'create'}">
         <c:forEach var="users" items="${users}"> 
                  <option value="${users.id}">${users.id}</OPTION>
        </c:forEach>   
     </c:when>
        <c:otherwise>    
          <c:forEach var="users" items="${users}"> 
              <c:choose>
            <c:when test="${users.id == param.assignedBy}">
               <option value="${users.id}" selected="">${users.id}</option>
            </c:when>
            <c:otherwise>
                <option value="${users.id}">${users.id}</option> 
            </c:otherwise>
                  </c:choose>
          </c:forEach>                             
        </c:otherwise>
   </c:choose>
    </select>
 <c:if test="${param['linkPressed'] == 'copy'}">
    <h3> <fmt:message key="label.manageschedule.selectWeek" /> </h3>
         <input type = "text" name ="copyScheduleDate_" id="copyScheduleDate" value="${copyScheduleDate_}"/><a href="javascript:{NewCal('copyScheduleDate','yyyymmdd','true','24')}"><img src="<c:url value='/img/cal.gif'/>" width="16" height="16" border="0" alt="Pick a date"></a>
   </c:if>
       
        <table class="borderAll" id="dataTable">
            <th></th>
            <th><fmt:message key="label.manageprogramslot.duration"/> </th>
            <th><fmt:message key="label.manageprogramslot.programName" /> </th>
             <th><fmt:message key="label.manageprogramslot.startTime" /> </th>
              <th><fmt:message key="label.manageprogramslot.dateOfProgram" /> </th>
        <c:if test="${param['linkPressed'] == 'create'}">
         <tr>
        <td>
         <input type="hidden" name="ins" value="create" />
        </td>
        <td>
            <input type ="text" name ="duration_" value="${duration_}" required/>
        </td>
        <td>
        <select name= "programName_">
                <c:forEach var="radioProgram" items="${rps}"> 
                            <option value="${radioProgram.name}">${radioProgram.name}</OPTION>
                </c:forEach>
            </select>
        </TD>
        <td><input type = "text" name ="startTime_" id="datepicker1" value="${startTime_}" required/><a href="javascript:{NewCal('datepicker1','yyyymmdd','true','24')}"><img src="<c:url value='/img/cal.gif'/>" width="16" height="16" border="0" alt="Pick a date"></a></td>
        <td> <input type = "text" name ="dateOfProgram_" id="datepicker2" value="${dateOfProgram_}" required/><a href="javascript:{NewCal('datepicker2','yyyymmdd','true','24')}"><img src="<c:url value='/img/cal.gif'/>" width="16" height="16" border="0" alt="Pick a date"></a></td>  
         </TR>
        </c:if>
      <c:if test="${param['linkPressed'] == 'modify'}">
         <tr>
        <td>
         <input type="hidden" name="ins" value="modify" />  
        </td>
         </tr>
           </c:if>
         <c:if test="${param['linkPressed'] == 'copy'}">
         <tr>
        <td>
         <input type="hidden" name="ins" value="copy" />  
        </td>
         </tr>
      </c:if>
          <c:forEach var="programSlot" items="${programSlot}" varStatus="status">
    <tr>
        <td>
              <input type= "hidden" name ="duration" value="${programSlot.duration}${programSlot.dateOfProgram}"/>
              <input type="hidden" name="ins" value="false" />
        </td>
        <td>
                <input name ="duration_${programSlot.duration}${programSlot.dateOfProgram}" value="${programSlot.duration}" readonly="readonly"/>
        </td>
        <td>
            <select name= "programName_${programSlot.duration}${programSlot.dateOfProgram}">
                <c:forEach var="radioProgram" items="${rps}"> 
                    <c:choose>
                        <c:when test="${radioProgram.name == programSlot.programName}">
                            <option value="${radioProgram.name}" selected="">${radioProgram.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${radioProgram.name}">${radioProgram.name}</OPTION>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </td>
        <td><input id="startTime_${programSlot.duration}${programSlot.dateOfProgram}" name ="startTime_${programSlot.duration}${programSlot.dateOfProgram}"  value="${programSlot.startTime}" required/> <a href="javascript:{NewCal('startTime_${programSlot.duration}${programSlot.dateOfProgram}','yyyymmdd','true','24')}"><img src="<c:url value='/img/cal.gif'/>" width="16" height="16" border="0" alt="Pick a date"></a></td></td>
        <td> <input name ="dateOfProgram_${programSlot.duration}${programSlot.dateOfProgram}" readonly="readonly" value="${programSlot.dateOfProgram}" /></td>    
        <td>

    </tr>
    </c:forEach>     
    </TABLE>
        <br></br>
    <input type="submit" value="Submit"/>
        </form>
  </body>
</HTML>
               
