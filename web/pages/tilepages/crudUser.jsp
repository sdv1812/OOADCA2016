<%-- 
    Document   : crudUser
    Created on : Sep 18, 2016, 1:56:56 PM
    Author     : Surenthiran T,Mohan
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
<fmt:setBundle basename="ApplicationResources" />
<title> <fmt:message key="title.crudUser"/> </title>
</head>
<body>
    
    <h1><fmt:message key="label.crudUser"/></h1>
        <c:url var="url" scope="page" value="/nocturne/addedituser">
            	<c:param name="id" value=""/>
                <c:param name="name" value=""/>
                <c:param name="insert" value="true"/>
        </c:url>
                <a href="${url}"><fmt:message key="label.crudUser.add"/></a>
        <br/><br/>
        <table class="borderAll">
            <tr>
                <th><fmt:message key="caption.User-Id"/></th>
                <th><fmt:message key="caption.User-Name"/></th>
              <th><fmt:message key="label.crudUser.edit"/>                        &nbsp;&nbsp;&nbsp;
 <fmt:message key="label.crudUser.delete"/></th> 
            </tr>
            <c:forEach var="crudUser" items="${users}" varStatus="status">
                <tr class="${status.index%2==0?'even':'odd'}">
                    <td class="nowrap">${crudUser.id}</td>
                    <td class="nowrap">${crudUser.name}</td>
                    <td class="nowrap">
                        <c:url var="updurl" scope="page" value="/nocturne/addedituser">
                            <c:param name="id" value="${crudUser.id}"/>
                            <c:param name="name" value="${crudUser.id}"/>
                            <c:param name="password" value="${crudUser.password}"/>
                            <c:param name="role" value="${crudUser.roles}"/>
                            <c:param name="insert" value="false"/>
                        </c:url>
                        <a href="${updurl}"><fmt:message key="label.crudUser.edit"/></a>
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        

                        <c:url var="delurl" scope="page" value="/nocturne/deleteuser">
                            <c:param name="name" value="${crudUser.id}"/>
                        </c:url>
                        <a href="${delurl}"><fmt:message key="label.crudUser.delete"/></a>
                    </td>
                </tr>
            </c:forEach>
                
        </table>      
                            
</body>
</html>