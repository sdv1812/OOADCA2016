<%-- 
    Document   : crudSchedule
    Created on : 22 Sep, 2016, 5:53:35 PM
    Author     : Srishti Miglani
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<Html>
    <Head>
               
        <c:if test="${not empty error}">
    <script>
   alert("${error}");
    </script>
</c:if>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
          <script language="javascript" type="text/javascript" src="<c:url value='/js/datetimepicker.js'/>"> </script>
    <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
    <fmt:setBundle basename="ApplicationResources" />
    <title> <fmt:message key="title.manageschedule"/> </title>    
    </Head>   
    <body>
         <h1><fmt:message key="label.manageschedule"/></h1>
    <c:url var="url" scope="page" value="/nocturne/setupschedule">
       <c:param name="linkPressed" value="create"/>
        </c:url> 
        
       
        <a href="${url}"><fmt:message key="label.manageschedule.add"/></a>
        <c:forEach var="manageschedule" items="${scs}">
          <h2>${manageschedule.key} </h2>
        <table class="borderAll">
            <tr>
                <th colspan = "4" align= "center" bgcolor="#e6e6e6">Week Schedule</TH>
            </tr>
            <tr>
                <th><fmt:message key="label.manageschedule.startDate"/></th>
                <th><fmt:message key="label.manageschedule.assignedBy"/></th>
                <th><fmt:message key="label.manageschedule.edit"/> <fmt:message key="label.manageschedule.delete"/></th>
                <th><fmt:message key="label.manageschedule.copy"/>
            </tr>
     
            
            <c:forEach var="weekvalues" items="${manageschedule.value}" varStatus="status">
                 <tr class="${status.index%2==0?'even':'odd'}">
                    <td class="nowrap">${weekvalues.startDate}</td>
                    <td class="nowrap">${weekvalues.assignedBy}</td>
                    <td class="nowrap">
                        <c:url var="updurl" scope="page" value="/nocturne/setupschedule">
                             <c:param name="startDate" value="${weekvalues.startDate}"/>
                             <c:param name="assignedBy" value="${weekvalues.assignedBy}"/>
                              <c:param name="linkPressed" value="modify"/>
                        </c:url>
                        <a href="${updurl}"><fmt:message key="label.manageschedule.edit"/></a>
                        &nbsp;&nbsp;&nbsp;
                        <c:url var="delurl" scope="page" value="/nocturne/deleteSchedule">
                            <c:param name="startDate" value="${weekvalues.startDate}"/>
                        </c:url>
                        <a href="${delurl}"><fmt:message key="label.manageschedule.delete"/></a>
                    </td>
                    <td class="nowrap">
                         <c:url var="copyurl" scope="page" value="/nocturne/setupschedule">
                            <c:param name="startDate" value="${weekvalues.startDate}"/>
                           <c:param name="assignedBy" value="${weekvalues.assignedBy}"/>
                           <c:param name="linkPressed" value="copy"/>
                        </c:url>
                        <a href="${copyurl}"><fmt:message key="label.manageschedule.copy"/></a>
                    </td>
                        
                </tr>
             </c:forEach>  
    </table>
        </c:forEach>
     
    </body>    
</Html>
