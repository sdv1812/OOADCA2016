/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.presenterproducer.delegate.ReviewSelectPPDelegate;
import sg.edu.nus.iss.phoenix.radioprogram.delegate.ReviewSelectProgramDelegate;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

/**
 *
 * @author Srishti Miglani
 */
@Action("setupschedule")
public class EnterScheduleDetailsCmd implements Perform {

    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        try {
            ScheduleDelegate del = new ScheduleDelegate();
            ReviewSelectProgramDelegate dele_radio = new ReviewSelectProgramDelegate();
            ReviewSelectPPDelegate del_user = new ReviewSelectPPDelegate();
            List<ProgramSlot> data = del.addEditSchedule(req.getParameter("startDate"));
            List<RadioProgram> radioProgram = dele_radio.reviewSelectRadioProgram();
            List<User> users = del_user.processSearch();
            req.setAttribute("users", users);
            req.setAttribute("rps", radioProgram);
            req.setAttribute("programSlot", data);
        } catch (SQLException ex) {
            Logger.getLogger(EnterScheduleDetailsCmd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/pages/setupschedule.jsp";
    }

}
