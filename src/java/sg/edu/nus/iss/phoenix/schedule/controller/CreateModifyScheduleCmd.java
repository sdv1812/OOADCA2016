/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.schedule.delegate.ReviewSelectScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;

/**
 *
 * @author Srishti Miglani
 */
@Action("modifyschedule")
public class CreateModifyScheduleCmd implements Perform {

    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        ReviewSelectScheduleDelegate del = new ReviewSelectScheduleDelegate();
        ScheduleDelegate del_schedule = new ScheduleDelegate();
        ProgramSlot programSlot = new ProgramSlot();

        String assignedBy = req.getParameter("users");
        String ins = (String) req.getParameter("ins");

        if (ins.equalsIgnoreCase("create")) {

            try {
                programSlot.setDuration(req.getParameter("duration_"));
                programSlot.setDateOfProgram(req.getParameter("dateOfProgram_"));
                programSlot.setProgramName(req.getParameter("programName_"));
                programSlot.setStartTime(req.getParameter("startTime_"));

                String ifOverlap = del_schedule.checkOverlap(req.getParameter("startTime_"), req.getParameter("duration_"));
                if (ifOverlap != null) {
                    req.setAttribute("error", ifOverlap);
                } else {
                    del_schedule.processCreate(programSlot, assignedBy);
                }
            } catch (SQLException ex) {
                Logger.getLogger(CreateModifyScheduleCmd.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (ins.equalsIgnoreCase("modify") || ins.equalsIgnoreCase("copy")) {
            for (String id : req.getParameterValues("duration")) {
                programSlot.setDuration(req.getParameter("duration_" + id));
                programSlot.setDateOfProgram(req.getParameter("dateOfProgram_" + id));
                programSlot.setProgramName(req.getParameter("programName_" + id));
                programSlot.setStartTime(req.getParameter("startTime_" + id));

                if (ins.equalsIgnoreCase("modify")) {
                    del_schedule.modifySchedule(programSlot, assignedBy);
                }
                if (ins.equalsIgnoreCase("copy")) {
                    try {
                        String targetStartDate = req.getParameter("copyScheduleDate_");
                        ProgramSlot copyprogramSlot = del_schedule.copySchedule(programSlot, targetStartDate);
                        String ifOverlap = del_schedule.checkOverlap(copyprogramSlot.getStartTime(), copyprogramSlot.getDuration());
                        if (ifOverlap != null) {
                            req.setAttribute("error", ifOverlap);
                        } else {
                            del_schedule.processCreate(copyprogramSlot, assignedBy);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(CreateModifyScheduleCmd.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        HashMap<Integer, List<WeeklySchedule>> data = del.reviewSelectSchedule();
        req.setAttribute("scs", data);
        return "/pages/crudSchedule.jsp";
    }

}
