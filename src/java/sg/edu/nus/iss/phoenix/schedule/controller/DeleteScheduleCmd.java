/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.schedule.delegate.ReviewSelectScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;

/**
 *
 * @author Sanskar Deepak
 */
@Action("deleteSchedule")
public class DeleteScheduleCmd implements Perform {

    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        ScheduleDelegate del = new ScheduleDelegate();
        String startDate = req.getParameter("startDate");
        del.processDelete(startDate);

        ReviewSelectScheduleDelegate rev_del = new ReviewSelectScheduleDelegate();
        HashMap<Integer, List<WeeklySchedule>> data = rev_del.reviewSelectSchedule();
        req.setAttribute("scs", data);
        return "/pages/crudSchedule.jsp";

    }
}
