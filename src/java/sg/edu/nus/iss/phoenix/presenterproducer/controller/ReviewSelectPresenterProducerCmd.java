/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.presenterproducer.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import sg.edu.nus.iss.phoenix.presenterproducer.delegate.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

/**
 *
 * @author Surethiran
 */
@Action("managepp")
public class ReviewSelectPresenterProducerCmd implements Perform{
    
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
    ReviewSelectPPDelegate ppdel =new ReviewSelectPPDelegate();
    List<User> data = ppdel.processSearch();
    req.setAttribute("Users",data);
    //return data;
    return "/pages/crudUser.jsp";
    }

}
