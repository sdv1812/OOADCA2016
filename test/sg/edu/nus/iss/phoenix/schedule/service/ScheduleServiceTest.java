/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.stubbing.OngoingStubbing;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

/**
 *
 * @author Sanskar
 */
public class ScheduleServiceTest {
       @Mock
       private ScheduleService service;
       private List<ProgramSlot> list_slots ;
        private ProgramSlot programSlot1;
     private ProgramSlot programSlot2; 
       
    public ScheduleServiceTest() {

    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws SQLException, NotFoundException {
       service = Mockito.mock(ScheduleService.class);
       programSlot1 = new ProgramSlot();
       programSlot2 = new ProgramSlot();
    
     programSlot1.setDateOfProgram("2016-09-11 00:00:00");
     programSlot1.setDuration("00:30:00");
     programSlot1.setProgramName("noose");
     programSlot1.setStartTime("2016-09-11 00:00:00");
     
     programSlot2.setDateOfProgram("2016-09-11 00:45:00");
     programSlot2.setDuration("00:45:00");
     programSlot2.setProgramName("noose"); 
     programSlot2.setStartTime("2016-09-11 00:45:00");
       
     list_slots=new ArrayList<ProgramSlot>();
     list_slots.add(programSlot1);
     list_slots.add(programSlot2);
     
    
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addEditSchedule method, of class ScheduleService.
     */
    @Test
    public void testAddEditSchedule() throws SQLException, NotFoundException {
        System.out.println("addEditSchedule");
        String date = "2016-09-05";
        when(service.addEditSchedule("2016-09-05")).thenReturn(list_slots);
        List<ProgramSlot> result = service.addEditSchedule(date);
        System.out.println(result.size());
        assertEquals(result.size(), 2);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of modifySchedule method, of class ScheduleService.
     */
    @Test(expected=Exception.class)
    public void testModifySchedule() {
        System.out.println("modifySchedule");
        
        programSlot1.setDateOfProgram("2016-09-11 00:15:00");
        programSlot1.setDuration("00:30:00");
        programSlot1.setProgramName("Srishti");
        programSlot1.setStartTime("2016-09-11 00:15:00");
        
        doThrow(new SQLException()).when(service).modifySchedule(programSlot1, "Srishti");
        // TODO review the generated test code and remove the default call to fail.

    }
    /**
     * Test of processDelete method, of class ScheduleService.
     */
    @Test
    public void testProcessDelete() throws SQLException {
        System.out.println("processDelete");
        
        String startDate = "2016-09-05 00:00:00";
        service.processDelete(startDate);
        when(service.addEditSchedule(startDate)).thenReturn(list_slots);
        List<ProgramSlot> result = service.addEditSchedule(startDate);
        System.out.println(result.size());
        assertNotEquals(result.size(), 0);
        
    }


    /**
     * Test of checkOverlap method, of class ScheduleService.
     */
    @Test
    public void testCheckOverlap() {
        System.out.println("checkOverlap");
        String startTime = "2016-09-11 00:00:00";
        String duration = "00:30:00";
        
        when(service.checkOverlap(startTime, duration)).thenReturn("true");
        String result = service.checkOverlap(startTime, duration);
        assertEquals("true", result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of checkExists method, of class ScheduleService.
     */
    @Test
    public void testCheckExists() {
        System.out.println("checkExists");
        String targetDate = "2016-09-05 00:00:00";
        OngoingStubbing<Boolean> thenReturn = when(service.checkExists(targetDate)).thenReturn(true);
        boolean expResult = true;
        boolean result = service.checkExists(targetDate);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail. 
    }
    
      /**
     * Test of processCreate method, of class ScheduleService.
     */

    public void testProcessCreate() throws SQLException {
        System.out.println("processCreate");
        doThrow(new SQLException()).when(service).processCreate(programSlot1,"Srishti");
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of copySchedule method, of class ScheduleService.
     */
    @Test
    public void testCopySchedule() {
        System.out.println("copySchedule");
        String targetStartDate = "2016-09-12 00:00:00";
        ProgramSlot expResult = new ProgramSlot();
        expResult.setDateOfProgram("2016-09-18 00:00:00");
        expResult.setDuration("00:30:00");
        expResult.setProgramName("noose");
        expResult.setStartTime("2016-09-18 00:00:00");
        when(service.copySchedule(programSlot1, targetStartDate)).thenReturn(expResult);
        ProgramSlot result = service.copySchedule(programSlot1, targetStartDate);
        assertEquals(expResult.getDateOfProgram(), result.getDateOfProgram());
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
