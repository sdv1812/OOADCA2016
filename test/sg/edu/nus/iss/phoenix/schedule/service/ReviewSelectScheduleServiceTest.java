/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;

/**
 *
 * @author Srishti Miglani
 */
public class ReviewSelectScheduleServiceTest {
    @Mock 
    private ReviewSelectScheduleService service;
    private ArrayList<WeeklySchedule> slots1;
     private ArrayList<WeeklySchedule> slots2;
    private WeeklySchedule week1;
    private WeeklySchedule week2;
     private WeeklySchedule week3;
    private WeeklySchedule week4;
    private HashMap<Integer,List<WeeklySchedule>> map;
     
    public ReviewSelectScheduleServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       service = Mockito.mock(ReviewSelectScheduleService.class);
       map = new HashMap<>();
       slots1 = new ArrayList<>();
       slots2 = new ArrayList<>();
       week1 = new WeeklySchedule();
       week1.setAssignedBy("Srishti");
       week1.setStartDate("2016-09-19 00:00:00");
       
       week2 = new WeeklySchedule();
       week2.setAssignedBy("Mohani");
       week2.setStartDate("2016-09-12 00:00:00");
       
       week3 = new WeeklySchedule();
       week3.setAssignedBy("Sanskar");
       week3.setStartDate("2017-09-11 00:00:00");
       
       week4 = new WeeklySchedule();
       week4.setAssignedBy("Koushik");
       week4.setStartDate("2017-09-18 00:00:00");
       
     slots1.add(week1);
     slots1.add(week2);
     slots2.add(week3);
     slots2.add(week4);
     
      map.put(2016,slots1);
      map.put(2017,slots2);  
       
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of reviewSelectSchedule method, of class ReviewSelectScheduleService.
     */
    @Test
    public void testReviewSelectSchedule() {
        System.out.println("reviewSelectSchedule");
        when(service.reviewSelectSchedule()).thenReturn(map);
        HashMap<Integer, List<WeeklySchedule>> expResult = map;
        HashMap<Integer, List<WeeklySchedule>> result = service.reviewSelectSchedule();
        
        assertFalse(result.values().isEmpty());
        assertEquals(expResult.keySet().size(), result.keySet().size());
    }
    
}
