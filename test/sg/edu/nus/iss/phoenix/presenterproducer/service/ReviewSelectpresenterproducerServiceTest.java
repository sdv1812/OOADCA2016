/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.presenterproducer.service;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import static org.mockito.Mockito.*;
import java.util.List;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.*;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactory;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 *
 * @author Surethiran,Koushik
 */
public class ReviewSelectpresenterproducerServiceTest {
    
   
    private UserDao uDao;
    private User user1,user2;
    private ArrayList<Role> roleUser1 = new ArrayList<>(); 
        private ArrayList<Role> roleUser2 = new ArrayList<>(); 

    private ArrayList<User> users = new ArrayList<>();
    public ReviewSelectpresenterproducerServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        uDao = mock(UserDao.class);
        user1 = new User();
        user2 = new User();
        user1.setName("Mohan");
        user1.setPassword("123");
        roleUser1.add(new Role("producer"));


        user1.setRoles(roleUser1);
        
        user2.setName("Suren");
        user2.setPassword("123");
        roleUser2.add(new Role("presenter"));

        user2.setRoles(roleUser2);
        users.add(user1);
        users.add(user2);
        when(uDao.searchByRole()).thenReturn(users);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of reviewSelectpresenterproducerService method, of class ReviewSelectpresenterproducerService.
     */
    @Test
    public void testReviewSelectpresenterproducerService() throws Exception {
        
        List<User> userList = uDao.searchByRole();
        assertEquals(userList.size(), 2);
        System.out.println(userList.toString());
        assertEquals("producer",userList.get(0).getRoles().listIterator().next().getRole());
        assertEquals("presenter",userList.get(1).getRoles().listIterator().next().getRole());

    }
    
}
